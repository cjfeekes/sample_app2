json.extract! client, :id, :name, :client_identifier, :primary_contact, :address_line_two, :zip, :state, :created_at, :updated_at
json.url client_url(client, format: :json)
