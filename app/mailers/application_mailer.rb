class ApplicationMailer < ActionMailer::Base
  default from: 'cjfeekes@gmail.com'
  layout 'mailer'
end
