class Client < ApplicationRecord
  before_save { self.client_identifier = client_identifier.upcase }
  
  
  validates :name, presence: true, length: { maximum: 50 }
  validates :client_identifier, presence: true, length:  { in: 4..20 },   # "in" stands for "inclusive" not "inbetween"--- min valid length is 4 and maximum valid is 20
                                                uniqueness: { case_sensitive: false }
                                                
  VALID_ZIP_REGEX =  /(^\d{5}(?:[\s]?[-\s][\s]?\d{4})?$)/
  validates :zip, :allow_blank => true, length: { in: 5..10 },  format: { with: VALID_ZIP_REGEX  }
  



end #end of class
