class SessionsController < ApplicationController

  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
#     if user.activated?
#        log_in user
#        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
#        redirect_back_or user
#      else
#        message  = "Account not activated. "
#        message += "Check your email for the activation link."
#        flash[:warning] = message
#        redirect_to root_url
#      end 

#   In the event you do not need account activation via email (or vice versa if the code above is blocked out), replace the if user.activated? block above (lines 9-18)
#  with the following for just logging the user in.  
      log_in user
      params[:session][:remember_me] == '1' ? remember(user) : forget(user)     
      redirect_to root_url
  #  redirect_back_or user


    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end