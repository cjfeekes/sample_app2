class AddContactphoneToClient < ActiveRecord::Migration[5.1]
  def change
    add_column :clients, :contactphone, :string
  end
end
