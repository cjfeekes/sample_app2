class AddIndexToClientsClientIdentifier < ActiveRecord::Migration[5.1]
  def change
  add_index :clients, :client_identifier, unique: true
  end
end
