class AddZipToClients < ActiveRecord::Migration[5.1]
  def change
    add_column :clients, :zip, :string
  end
end
