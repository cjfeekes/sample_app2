class RemoveZipFromClients < ActiveRecord::Migration[5.1]
  def change
    remove_column :clients, :zip, :integer
  end
end
