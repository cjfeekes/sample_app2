class CreateClients < ActiveRecord::Migration[5.1]
  def change
    create_table :clients do |t|
      t.string :name
      t.string :client_identifier
      t.string :primary_contact
      t.string :address_line_two
      t.integer :zip
      t.string :state

      t.timestamps
    end
  end
end
