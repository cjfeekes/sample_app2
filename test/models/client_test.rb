require 'test_helper'

class ClientTest < ActiveSupport::TestCase

  def setup
    @client = Client.new(name: "Example Client", client_identifier: "MSFT", 
                        address_line_two: "2536 Longmire", state: "Washington", city: "Issaquah",
                        zip: 98029, primary_contact: "Example contact")
  end ##end of setup
  
  
  test "Client should be valid" do
    assert @client.valid?
   
  end

  test "Client name should be present" do
    @client.name = "     "
    assert_not @client.valid?
  end


  test "client_identifier should be present" do
    @client.client_identifier = "     "
    assert_not @client.valid?
  end

   test "client name should not be too long" do
    @client.name = "a" * 51
    assert_not @client.valid?
  end


   test "client identifier should not be too long" do
    @client.client_identifier = "a" * 21
    assert_not @client.valid?
  end
     
    test "client identifier should not be too short" do
    @client.client_identifier = "a" * 3
    assert_not @client.valid?
  end





end #end of class